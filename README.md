# 6G-ANNA-oai-rf-simulator

To apply the advanced channel convolution to [Openairinterface5G](https://gitlab.eurecom.fr/oai/openairinterface5g) in version `2022.w41` use the following command
```sh
git apply --whitespace=fix --reject channel-conv.patch
```

Put the corresponding UL- and DL channel matrices in the files channel/ul_channelmatrix and channel/dl_channelmatrix. Examples for channel matrices can be found in the channels/ folder.

If you want to use your own channel matrix A, it has to be in the following form.

Re(A) -Im(A);

Im(A) Re(A)